import mysql from 'mysql'
import util from 'util'
import {config} from 'dotenv'

config()

const configdb = {
    host:process.env.HOST_DB,
    port: process.env.PORT_DB,
    user: process.env.USER_DB,
    password: process.env.PASSWORD_DB,
    database:process.env.NAME_DB,
    charset: 'utf8mb4_unicode_ci'
}

const conexion = mysql.createConnection(configdb)


export const db= {
    connect : ()=>{
        return util.promisify(conexion.connect)
    },
    query : (sql,args)=>{
        return util.promisify(conexion.query).call(conexion,sql,args)
    }
}


