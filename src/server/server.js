import express from 'express'
import cors from 'cors'

import examenMedicoRoutes from '../routes/examenMedico.routes'
import calendarioRoutes from '../routes/calendario.routes'
import horarioRoutes from '../routes/horario.routes'
import tipoReservaRoutes from '../routes/tipoReserva.routes'
import reservaRouter from '../routes/reserva.routes'
import formaPagoRouter from '../routes/formaPago.routes'
import usuarioRouter from '../routes/usuario.routes'
import consultorioRouter from '../routes/consultorio.routes'

const app = express()
//SETTINGS
app.set("port", process.env.PORT || 3000)
//MIDDLEWARES
app.use(express.json())
app.use(cors())
//ROUTES
app.use(examenMedicoRoutes)
app.use(calendarioRoutes)
app.use(horarioRoutes)
app.use(tipoReservaRoutes)
app.use(reservaRouter)
app.use(formaPagoRouter)
app.use(usuarioRouter)
app.use(consultorioRouter)

export default app