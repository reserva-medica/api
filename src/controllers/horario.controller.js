import { db } from "../server/database"

export const horarioController = {
    listarHorariosNoDisponiblePorTipoDeReserva: async (req,res)=>{
        
        const opcion = 2
        const {idTipoReserva,idEscogido,anio,mes,semana} = req.body
        try {
            const rows = await db.query('CALL MANTENIMIENTO_FECHANODISPONIBLE(?,?,?,?,?,?,?,?)',[opcion,0,idTipoReserva,idEscogido,anio,mes,semana,0])
           
            res.json({
                status: true,
                data: rows[0],
                mensaje: ''
            })
        } catch (e) {
            
            res.json({
                status: false,
                data: [],
                mensaje: e.toString()
            })
        }
    },
    registrarHorarioNoDisponiblePorTipoDeReserva: async (req,res)=>{
        
        // const {horarios} = req.body
        console.log("---body----")
        console.log(req.body)
        const horarios = req.body
        try {
            // const data = await db.query('CALL MANTENIMIENTO_FECHANODISPONIBLE(?,?,?,?,?,?,?,?)',[opcion,0,idTipoReserva,idEscogido,anio,mes,semana,dia])

            // const idFechaNoDisponible = data[0][0].id
            const peticionesFecha = []
            
            horarios.forEach(horario=>{
                const peticion = db.query('CALL MANTENIMIENTO_FECHANODISPONIBLE(?,?,?,?,?,?,?,?)',[horario.opcion,0,horario.idTipoReserva,horario.idEscogido,horario.anio,horario.mes,horario.semana,horario.dia])
                peticionesFecha.push(peticion)
            })

            const data = await Promise.all(peticionesFecha)

            const peticionesHorario = []
            data.forEach((data,index)=>{
                const id = data[0][0].id
                horarios[index].horarios.forEach(h=>{
                    const peticion = db.query('CALL MANTENIMIENTO_HORARIO(?,?,?,?,?)',[1,0,id,h.horaInicio,h.horaFin])
                    peticionesHorario.push(peticion)
                })
            })
            
            await Promise.all(peticionesHorario)

            // const rows = await db.query('CALL MANTENIMIENTO_HORARIO(?,?,?,?,?)',[opcion,0,idFechaNoDisponible,horaInicio,horaFin])
            
            res.json({
                status: true,
                data: [],
                mensaje: ''
            })
        } catch (e) {
            
            res.json({
                status: false,
                data: [],
                mensaje: e.toString()
            })
        }
    },
    eliminarTodosLosHorariosporTipoReserva: async(req,res)=>{
        const opcion = 0
        const {idTipoReserva,idEscogido,anio,mes,semana,dia} = req.body
       
        try {

            // const rows = await db.query('CALL MANTENIMIENTO_HORARIO(?,?,?,?,?,?)',[opcion,0,idFechaNoDisponible,'',''])
            const data = await db.query('CALL MANTENIMIENTO_FECHANODISPONIBLE(?,?,?,?,?,?,?,?)',[opcion,0,idTipoReserva,idEscogido,anio,mes,semana,dia])
            res.json({
                status: true,
                data: [],
                mensaje: ''
            })
        } catch (e) {
            
            res.json({
                status: false,
                data: [],
                mensaje: e.toString()
            })
        }
    }

}