
import  {db}  from '../server/database' 

export const examenMedicoController = {
    listarExamenes: async (req,res)=>{
        //console.log(req.query)
        const opcion = 1
        try{
            
            const rows =  await db.query('CALL MANTENIMIENTO_EXAMEN(?,?,?,?)',[opcion,0,'',0.0])
            //console.log(rows[0])
            res.json({
                status: true,
                data: rows[0],
                mensaje:''
            })
        }catch(e){
            console.log(e)
            res.json({
                status: false,
                data: [],
                mensaje: e.toString()
            })
            
        }
    },
    registrarExamen: async (req,res)=>{

        const {opcion,id=0,descripcion='',monto=0.0} = req.body

        //console.log(req.body)
        try{
            
            const rows =  await db.query('CALL MANTENIMIENTO_EXAMEN(?,?,?,?)',[opcion,id,descripcion,monto])

            // const idEscogido = rows[0][0].idExamen
            // console.log(idEscogido)
            //se registran los horarios por default sin esperar a q termine 
            // db.query('CALL MANTENIMIENTO_HORARIODEFAULT(?,?)',[1,idEscogido])
            //console.log(rows)
            res.json({
                status:true,
                data:[],
                mensaje:''
            })
        }catch(e){
            //console.log(e)
            res.json({
                status: false,
                data: [],
                mensaje: e.toString()
            })
        }
    },
    actualizarExamen: async(req,res)=>{
       
        const {opcion,descripcion='',monto=0.0} = req.body
        const {id} = req.params
        try{
            
            const rows =  await db.query('CALL MANTENIMIENTO_EXAMEN(?,?,?,?)',[opcion,id,descripcion,monto])
            //console.log(rows)
            res.json({
                status:true,
                data:[],
                mensaje:''
            })
        }catch(e){
            //console.log(e)
            res.json({
                status: false,
                data: [],
                mensaje: e.toString()
            })
        }
    },
    eliminarExamen: async(req,res)=>{
        const {opcion,descripcion='',monto=0.0} = req.body
        const {id} = req.params
        try{
            
            const rows =  await db.query('CALL MANTENIMIENTO_EXAMEN(?,?,?,?)',[opcion,id,descripcion,monto])
            //console.log(rows)
            res.json({
                status:true,
                data:[],
                mensaje:''
            })
        }catch(e){
            //console.log(e)
            res.json({
                status: false,
                data: [],
                mensaje: e.toString()
            })
        }
    }

}