
import { formatoFechaBD} from "../helpers/fechas"
import { db } from "../server/database"

export const calendarioController = {
    registrarDiaNoLaborable: async(req,res)=>{
        
        const {opcion,id=0,fecha='',habilitado='1'}= req.body
        //const fechaBD = formatoFechaBD(fecha)
       
        try{

            const rows = await db.query('CALL MANTENIMIENTO_CALENDARIO(?,?,?,?)',[opcion,id,fecha,habilitado])
            res.json({
                status:true,
                data:[],
                mensaje:''
            })
        }catch(e){
            res.json({
                status:false,
                data:[],
                mensaje:e.toString()
            })
        }
    },
    obtenerDiasNoLaborables: async(req,res)=>{
        
        const opcion = 1
        const {fecha=''} = req.query
        // const fechaBD = formatoFechaBD(fecha)

        try{

            const rows = await db.query('CALL MANTENIMIENTO_CALENDARIO(?,?,?,?)',[opcion,0,fecha,'0'])
            const data = []
            data.push({
                calendarioNoLaborable: rows[0]
            })
            res.json({
                status:true,
                data,
                mensaje:''
            })
        }catch(e){
            console.log(e)
            res.json({
                status:false,
                data:[],
                mensaje:e.toString()
            })
        }
    }
}