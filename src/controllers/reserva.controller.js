
import { diasDeSemana } from "../helpers/fechas"
import { db } from "../server/database"

export const reservaController = {
    registrarNuevaReserva: async (req,res)=>{
        const {opcion,idReserva,idTipoReserva,idEscogido,fechaTemporal,estado="I"} = req.body
        try {

            let idExamen = 0
            let idUsuario = 0
            let idConsultorio = 0

            if(idTipoReserva==1){
                idExamen = idEscogido
            }
            if(idTipoReserva==2){
                idUsuario = idEscogido
            }
            if(idTipoReserva==3){
                idConsultorio = idEscogido
            }
            
            const dataReserva = await db.query('CALL MANTENIMIENTO_RESERVA(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',[opcion,idReserva,idTipoReserva,0,fechaTemporal,fechaTemporal,'','',0,fechaTemporal,0,0.0,idExamen,idUsuario,idConsultorio,estado])
            //console.log(dataReserva)
            res.json({
                status: true,
                data: dataReserva[0],
                mensaje:''
            })
        } catch (e) {
            console.log(e)
            res.json({
                status: false,
                data: [],
                mensaje: e.toString()
            })
        }
    },
    registrarClienteParcial: async(req,res)=>{

        const {opcion,nombre,apellido,email,telefono,idTipoReserva,idEscogido,fechaTemporal,estado="I"} = req.body
        try {

            let idExamen = 0
            let idUsuario = 0
            let idConsultorio = 0

            if(idTipoReserva==1){
                idExamen = idEscogido
            }
            if(idTipoReserva==2){
                idUsuario = idEscogido
            }
            if(idTipoReserva==3){
                idConsultorio = idEscogido
            }

            const dataCliente = await db.query('CALL MANTENIMIENTO_CLIENTE(?,?,?,?,?,?,?,?,?,?,?)',[opcion,0,nombre,apellido,email,telefono,'','',0,'',''])
           
            const idCliente = dataCliente[0][0].idCliente
            
            const dataReserva = await db.query('CALL MANTENIMIENTO_RESERVA(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',[opcion,0,idTipoReserva,idCliente,fechaTemporal,fechaTemporal,'','',0,fechaTemporal,0,0.0,idExamen,idUsuario,idConsultorio,estado])
            //console.log(dataReserva)
            res.json({
                status: true,
                data: dataReserva[0],
                mensaje:''
            })
        } catch (e) {
            console.log(e)
            res.json({
                status: false,
                data: [],
                mensaje: e.toString()
            })
        }
    },
    registrarReserva: async (req,res)=>{
        const {opcion,idReserva,idTipoReserva,fechaReserva,horaInicio,horaFin,idDescuento,idEscogido,estado='I',dni,distrito,edad,ruc,razonSocial,fechaTemporal} = req.body

        try {

            let idExamen = 0
            let idUsuario = 0
            let idConsultorio = 0

            if(idTipoReserva==1){
                idExamen = idEscogido
            }
            if(idTipoReserva==2){
                idUsuario = idEscogido
            }
            if(idTipoReserva==3){
                idConsultorio = idEscogido
            }
            const data = await db.query('CALL MANTENIMIENTO_RESERVA(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',[opcion,idReserva,idTipoReserva,0,fechaTemporal,fechaReserva,horaInicio,horaFin,0,fechaReserva,idDescuento,0.0,idExamen,idUsuario,idConsultorio,estado])
            console.log(data)
            const idCliente = data[0][0].idCliente

           db.query('CALL MANTENIMIENTO_CLIENTE(?,?,?,?,?,?,?,?,?,?,?)',[opcion,idCliente,'','','','',dni,distrito,edad,ruc,razonSocial])

            res.json({
                status:true,
                data:[],
                mensaje:''
            })
            
        } catch (e) {
            
            res.json({
                status:false,
                data:[],
                mensaje: e.toString()
            })
        }
    },
    obtenerFechasNoDisponiblesParaExamenes: async (req,res)=>{
        const {idTipoReserva,idEscogido,fecha} = req.body
        try{
            console.log(req.body)
            const dataBD = await db.query('CALL MANTENIMIENTO_OBTENERFECHASDISPONIBLES(?,?,?)',[idTipoReserva,idEscogido,fecha])
            const fechasSegunReserva = dataBD[0]
            const diasSegunHorario = dataBD[1]
            const fechaSegunCalendario = await db.query('CALL MANTENIMIENTO_CALENDARIO(?,?,?,?)',[1,0,fecha,'1'])

            // const diasSegunHorario = await db.query('CALL MANTENIMIENTO_OBTENERHORARIONODISPONIBLE(?,?)',[idTipoReserva,idEscogido])
            console.log(fechasSegunReserva)
            console.log(diasSegunHorario)
            const data = []
            const fechaNoDisponible = []
            const diaNoDisponible = []
            // estas fechas si o si estaran habilitadas
            // segun del punto de vista de la tabla CalendarioNoDisponible
            // lo habilitado significaria fecha no disponible
            fechasSegunReserva.forEach(item=>{
                fechaNoDisponible.push({
                    fecha:item.fechaReserva,
                    habilitado: '1' 
                })
            })


            fechaSegunCalendario[0].forEach(item=>{
               
                fechaNoDisponible.push({
                    fecha: item.fecha,
                    habilitado : item.habilitado
                })
            })
        
            diasSegunHorario.forEach(item=>{
                console.log(item)
                const anio = item.anio
                const mes = item.mes
                const semana = item.semana
                const diaBD = item.dia
                const primerFecha = new Date(anio,mes-1,1,0,0,0,0)
              
                // // const ultimaFecha = new Date(anio,mes,1,0,0,0,0)
                let contSemanas = 1
                let termina = false
                while(!termina){
                    if(semana===contSemanas){
                        termina = true
               
                        while(semana===contSemanas){
                            // primerFecha.setDate(primerFecha.getDate()+1)
                            const dia = primerFecha.getDay()
                            if(dia===diaBD){

                                contSemanas++
                            }else{
                                primerFecha.setDate(primerFecha.getDate()+1)
                            }
                        }
                    }else{
                        primerFecha.setDate(primerFecha.getDate()+1)
                        const dia = primerFecha.getDay()
                        if(dia===0){
                            contSemanas++
                        }
                    }
                
                }
                console.log(primerFecha.toISOString().split("T")[0])
                const fechaBloqueada = primerFecha.toISOString().split("T")[0]
                // if(diasDeSemana.includes(item.dia)){
                //     diaNoDisponible.push({
                //         dia: diasDeSemana.indexOf(item.dia)
                //     })
                // }
                const existe = fechaNoDisponible.filter(item => item.fecha == fechaBloqueada)
                if(existe.length==0){
                    fechaNoDisponible.push({
                        fecha:fechaBloqueada,
                        habilitado: item.cantidadIntervalosNoDisponibles >= item.cantidadIntervalosTotal ? '1': '0'
                    })
                }
                
            })
            data.push({
                fechaNoDisponible,
                diaNoDisponible
            })
            // console.log(fechasSegunReserva)
            // console.log(fechaSegunCalendario)

            res.json({
                status: true,
                data,
                mensaje:''
            })
        }catch(e){
            res.json({
                status: false,
                data: [],
                mensaje: e.toString()
            })
        }
    },

    obtenerHorariosNoDisponiblesSegunFechaElegida: async (req,res)=>{
        const {idTipoReserva,idEscogido,fecha} = req.body
        try {

            const data = await db.query('CALL MANTENIMIENTO_OBTENERHORARIONODISPONIBLEPORFECHAELEGIDA(?,?,?)',[idTipoReserva,idEscogido,fecha])
            
            const horarios = []
            const horariosNoDisponiblesSegunFechaElegida = []
            const horariosNoDisponiblesSegunConfigHorario = []
            
            data[0].forEach(item=>{
               
                horarios.push({
                    horaInicio: item.horaInicio,
                    horaFin:item.horaFin
                })
            })
            data[1].forEach(item=>{
               
                horarios.push({
                    horaInicio: item.horaInicio,
                    horaFin: item.horaFin
                })
            })
            // console.log(horarios)
            
            res.json({
                status: true,
                data: horarios,
                mensaje:''
            })
        } catch (e) {
            res.json({
                status: false,
                data: [],
                mensaje: e.toString()
            })
        }
    },
    obtenerPagos: async (req,res)=>{
       console.log(req.body)
        const {reservas,fecha,estado} = req.body
        try {
            const arregloPromesas = []

            reservas.forEach(idReserva=>{
                arregloPromesas.push(db.query('CALL MANTENIMIENTO_RESERVA(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',[3,idReserva,0,0,fecha,fecha,'','',0,fecha,0,0.0,0,0,0,estado]))
            })

            const datos = await Promise.all(arregloPromesas)
            const pagos = []
            datos.forEach(dato=>{
                const pago = dato[0][0]
                pagos.push(pago)
            })
        // const data = await db.query('CALL MANTENIMIENTO_RESERVA(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',[3,idReserva,0,0,fecha,fecha,'','',0,fecha,0,0.0,0,0,0,estado])
        // console.log(data)
        //     const idCliente = data[0][0].idCliente

        //    db.query('CALL MANTENIMIENTO_CLIENTE(?,?,?,?,?,?,?,?,?,?,?)',[opcion,idCliente,'','','','',dni,distrito,edad,ruc,razonSocial])

            res.json({
                status:true,
                data:pagos,
                mensaje:''
            })
            
        } catch (e) {
            
            res.json({
                status:false,
                data:[],
                mensaje: e.toString()
            })
        }
    },
    obtenerReservas: async(req,res)=>{
        const {fechaInicio,fechaFin,cEstado=''} = req.body
        try {

            const data = await db.query('CALL MANTENIMIENTO_CONSULTAS(?,?,?)',[fechaInicio,fechaFin,cEstado])
            
           
            // console.log(horarios)
            
            res.json({
                status: true,
                data: data[0],
                mensaje:''
            })
        } catch (e) {
            res.json({
                status: false,
                data: [],
                mensaje: e.toString()
            })
        }
    },
    realizarPago: async (req,res)=>{
        const {reservas,fecha,idFormaPago} = req.body
        console.log(req.body)
        try {
            const arregloPromesas = []

            reservas.forEach(idReserva=>{
                arregloPromesas.push(db.query('CALL MANTENIMIENTO_RESERVA(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',[4,idReserva,0,0,fecha,fecha,'','',idFormaPago,fecha,0,0.0,0,0,0,'C']))
            })

            await Promise.all(arregloPromesas)
            
       

            res.json({
                status:true,
                data:[],
                mensaje:''
            })
            
        } catch (e) {
            
            res.json({
                status:false,
                data:[],
                mensaje: e.toString()
            })
        }
    }

}