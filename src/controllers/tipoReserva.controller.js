import { db } from "../server/database"

export const tipoReservaController = {
    listarTipoReserva: async (req,res)=>{
        const opcion = 1

        try {
            const rows =  await db.query('CALL MANTENIMIENTO_TIPORESERVA(?,?,?,?,?,?,?)',[opcion,0,'',0,0,'',''])
            //console.log(rows[0])
            res.json({
                status: true,
                data: rows[0],
                mensaje:''
            })
        } catch (e) {
            res.json({
                status: false,
                data: [],
                mensaje: e.toString()
            })
        }
    },
    listarTipoReservaSegunEscogido: async(req,res)=>{
        const opcion = 1
        const {id} = req.params
       // console.log(req.params)
        try {
            const rows =  await db.query('CALL MANTENIMIENTO_TIPORESERVA(?,?,?,?,?,?,?)',[opcion,id,'',0,0,'',''])
         //   console.log(rows)
            //console.log(rows[0])
            res.json({
                status: true,
                data: rows[0],
                mensaje:''
            })
        } catch (e) {
            res.json({
                status: false,
                data: [],
                mensaje: e.toString()
            })
        }
    },
    registrarTipoReserva: async (req,res)=>{

        const {opcion,id=0,descripcion='',intervalo=0,cantidad=0,rangoInicio='',rangoFinal=''} = req.body
        try {
            const rows = await db.query('CALL MANTENIMIENTO_TIPORESERVA(?,?,?,?,?,?,?)',[opcion,id,descripcion,intervalo,cantidad,rangoInicio,rangoFinal])

            res.json({
                status: true,
                data: [],
                mensaje:''
            })

        } catch (e) {
            res.json({
                status: false,
                data: [],
                mensaje: e.toString()
            })
        }
    },
    actualizarTipoReserva: async (req,res)=> {
        
        const {opcion,descripcion='',intervalo=0,cantidad=0} = req.body
        const {id} = req.params

        try {
            const rows = await db.query('CALL MANTENIMIENTO_TIPORESERVA(?,?,?,?,?,?,?)',[opcion,id,descripcion,intervalo,cantidad,'',''])

            res.json({
                status: true,
                data: [],
                mensaje:''
            })
        } catch (e) {
            res.json({
                status: false,
                data: [],
                mensaje: e.toString()
            })
        }
    },
    listarRegistrosPorTipoReserva: async (req,res)=>{
        const {opcion} = req.params

        try {
            const rows = await db.query('CALL LISTADO_TIPORESERVA(?)',[opcion])

            res.json({
                status: true,
                data: rows[0],
                mensaje:''
            })
        } catch (e) {
            res.json({
                status: false,
                data: [],
                mensaje: e.toString()
            })
        }
    }
}