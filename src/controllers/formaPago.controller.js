import { db } from "../server/database"

export const formaPagoController = {
    obtenerFormaPagos: async(req,res)=>{
        
        const opcion = 3
        //const fechaBD = formatoFechaBD(fecha)
       
        try{

            const rows = await db.query('CALL MANTENIMIENTO_FORMAPAGO(?,?,?,?)',[opcion,0,'','1'])
            res.json({
                status:true,
                data:rows[0],
                mensaje:''
            })
        }catch(e){
            res.json({
                status:false,
                data:[],
                mensaje:e.toString()
            })
        }
    }
}