
import { db } from "../server/database"

export const consultorioController = {
    registrarTipoConsultorio: async (req,res)=>{
        const {opcion,idTipoConsultorio=0,descripcion} = req.body

        try{

            await db.query('CALL MANTENIMIENTO_TIPOCONSULTORIO(?,?,?)',[opcion,idTipoConsultorio,descripcion])

            res.json({
                status:true,
                data:[],
                mensaje:''
            })
        }catch(e){
            res.json({
                status:false,
                data:[],
                mensaje:e.toString()
            })
        }
    },
    registrarConsultorio: async(req,res)=>{
        const {opcion,idConsultorio=0,nombre,monto,tipoConsultorio:idTipoConsultorio} = req.body

        try{

            await db.query('CALL MANTENIMIENTO_CONSULTORIO(?,?,?,?,?)',[opcion,idConsultorio,nombre,monto,idTipoConsultorio])

            res.json({
                status:true,
                data:[],
                mensaje:''
            })
        }catch(e){
            res.json({
                status:false,
                data:[],
                mensaje:e.toString()
            })
        }
    },
    obtenerTipoConsultorios: async (req,res)=>{
        const opcion = 1
      
        try{

            const data= await db.query('CALL MANTENIMIENTO_TIPOCONSULTORIO(?,?,?)',[opcion,0,''])


            res.json({
                status:true,
                data:data[0],
                mensaje:''
            })
        }catch(e){
            res.json({
                status:false,
                data:[],
                mensaje:e.toString()
            })
        }
    },
    obtenerConsultorios: async (req,res)=>{
        const opcion = 1
      
        try{

            const data= await db.query('CALL MANTENIMIENTO_CONSULTORIO(?,?,?,?,?)',[opcion,0,'',0.0,0])


            res.json({
                status:true,
                data:data[0],
                mensaje:''
            })
        }catch(e){
            res.json({
                status:false,
                data:[],
                mensaje:e.toString()
            })
        }
    }
}