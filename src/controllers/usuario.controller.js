import { db } from "../server/database"

export const usuarioController={


    obtenerRoles: async(req,res)=>{
        const opcion = 1

        try{

            const data = await db.query('CALL MANTENIMIENTO_ROLES(?,?,?,?)',[opcion,0,'',''])

            res.json({
                status:true,
                data:data[0],
                mensaje:''
            })
        }catch(e){
            res.json({
                status:false,
                data:[],
                mensaje:e.toString()
            })
        }
    },
    obtenerModalidades: async(req,res)=>{
        const opcion = 0
      
        try{

            const data= await db.query('CALL MANTENIMIENTO_ESPECIALIDAD(?,?,?,?)',[opcion,0,'',0.0])


            res.json({
                status:true,
                data:data[0],
                mensaje:''
            })
        }catch(e){
            res.json({
                status:false,
                data:[],
                mensaje:e.toString()
            })
        }
    },
    obtenerEspecialidades: async (req,res)=>{
        const opcion = 1

        try{

            const data= await db.query('CALL MANTENIMIENTO_ESPECIALIDAD(?,?,?,?)',[opcion,0,'',0.0])

            res.json({
                status:true,
                data:data[0],
                mensaje:''
            })
        }catch(e){
            res.json({
                status:false,
                data:[],
                mensaje:e.toString()
            })
        }
    } ,
    
    registrarEspecialidad: async (req,res)=>{

        const {opcion,idEspecialidad=0,descripcion,monto} = req.body

        try{

            await db.query('CALL MANTENIMIENTO_ESPECIALIDAD(?,?,?,?)',[opcion,idEspecialidad,descripcion,monto])

            res.json({
                status:true,
                data:[],
                mensaje:''
            })
        }catch(e){
            res.json({
                status:false,
                data:[],
                mensaje:e.toString()
            })
        }


    },
    registrarUsuario: async(req,res)=>{


        const {opcion,idUsuario=0,nombre,apellido,descripcion,usuario,contrasena,email,rol:idRol,especialidad:idEspecialidad,modalidad:idModalidad} = req.body
        console.log(req.body)
        try{

            await db.query('CALL MANTENIMIENTO_USUARIO(?,?,?,?,?,?,?,?,?,?,?)',[opcion,idUsuario,nombre,apellido,descripcion,usuario,contrasena,email,idRol,idEspecialidad,idModalidad])

            res.json({
                status:true,
                data:[],
                mensaje:''
            })
        }catch(e){
            res.json({
                status:false,
                data:[],
                mensaje:e.toString()
            })
        }

    }
}