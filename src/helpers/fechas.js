

export const formatoFechaBD = (fecha)=>{

    const anio = fecha.split('/')[2]
    const mes  = fecha.split('/')[1]
    const dia  = fecha.split('/')[0]

    return `${anio}-${mes}-${dia}`

}

export const diasDeSemana = ['domingo','lunes','martes','miercoles','jueves','viernes','sabado']