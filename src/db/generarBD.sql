CREATE DATABASE  IF NOT EXISTS `ReservaMedica` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `ReservaMedica`;
-- MySQL dump 10.13  Distrib 8.0.22, for Linux (x86_64)
--
-- Host: localhost    Database: ReservaMedica
-- ------------------------------------------------------
-- Server version	8.0.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `CalendarioNoLaborable`
--

DROP TABLE IF EXISTS `CalendarioNoLaborable`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `CalendarioNoLaborable` (
  `idCalendarioNoLaborable` int NOT NULL AUTO_INCREMENT,
  `fecha` date NOT NULL,
  `habilitado` char(1) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`idCalendarioNoLaborable`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Cliente`
--

DROP TABLE IF EXISTS `Cliente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Cliente` (
  `idCliente` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `apellido` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telefono` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dni` varchar(8) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distrito` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `edad` int DEFAULT NULL,
  `ruc` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `razonSocial` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`idCliente`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Consultorio`
--

DROP TABLE IF EXISTS `Consultorio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Consultorio` (
  `idConsultorio` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `monto` decimal(8,2) NOT NULL,
  `idTipoConsultorio` int NOT NULL,
  PRIMARY KEY (`idConsultorio`),
  KEY `fk_Consultorio_TipoConsultorio_idx` (`idTipoConsultorio`),
  CONSTRAINT `fk_Consultorio_TipoConsultorio` FOREIGN KEY (`idTipoConsultorio`) REFERENCES `TipoConsultorio` (`idTipoConsultorio`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Descuento`
--

DROP TABLE IF EXISTS `Descuento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Descuento` (
  `idDescuento` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descripcion` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fechaInicio` date DEFAULT NULL,
  `fechaFin` date DEFAULT NULL,
  `valor` decimal(5,2) NOT NULL,
  `habilitado` char(1) COLLATE utf8mb4_unicode_ci NOT NULL,
  `idTipoReserva` int NOT NULL,
  `tipoDescuento` char(1) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`idDescuento`),
  KEY `fk_Descuento_TipoReserva_idx` (`idTipoReserva`),
  CONSTRAINT `fk_Descuento_TipoReserva` FOREIGN KEY (`idTipoReserva`) REFERENCES `TipoReserva` (`idTipoReserva`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Especialidad`
--

DROP TABLE IF EXISTS `Especialidad`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Especialidad` (
  `idEspecialidad` int NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `monto` decimal(8,2) NOT NULL,
  PRIMARY KEY (`idEspecialidad`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Examen`
--

DROP TABLE IF EXISTS `Examen`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Examen` (
  `idExamen` int NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `monto` decimal(8,2) NOT NULL,
  PRIMARY KEY (`idExamen`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `FechaNoDisponible`
--

DROP TABLE IF EXISTS `FechaNoDisponible`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `FechaNoDisponible` (
  `idFechaNoDisponible` int NOT NULL AUTO_INCREMENT,
  `idTipoReserva` int NOT NULL,
  `idExamen` int DEFAULT NULL,
  `idUsuario` int DEFAULT NULL,
  `idConsultorio` int DEFAULT NULL,
  `anio` int NOT NULL,
  `mes` int NOT NULL,
  `semana` int NOT NULL,
  `dia` int NOT NULL,
  PRIMARY KEY (`idFechaNoDisponible`),
  KEY `fk_FechaNoDisponible_TipoReserva_idx` (`idTipoReserva`),
  KEY `fk_FechaNoDisponible_Examen_idx` (`idExamen`),
  KEY `fk_FechaNoDisponible_Medico_idx` (`idUsuario`),
  KEY `fk_FechaNoDisponible_Consultorio_idx` (`idConsultorio`),
  CONSTRAINT `fk_FechaNoDisponible_Consultorio` FOREIGN KEY (`idConsultorio`) REFERENCES `Consultorio` (`idConsultorio`),
  CONSTRAINT `fk_FechaNoDisponible_Examen` FOREIGN KEY (`idExamen`) REFERENCES `Examen` (`idExamen`),
  CONSTRAINT `fk_FechaNoDisponible_Medico` FOREIGN KEY (`idUsuario`) REFERENCES `Usuario` (`idUsuario`),
  CONSTRAINT `fk_FechaNoDisponible_TipoReserva` FOREIGN KEY (`idTipoReserva`) REFERENCES `TipoReserva` (`idTipoReserva`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `FormaPago`
--

DROP TABLE IF EXISTS `FormaPago`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `FormaPago` (
  `idFormaPago` int NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `habilitado` char(1) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`idFormaPago`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `HorarioNoDisponible`
--

DROP TABLE IF EXISTS `HorarioNoDisponible`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `HorarioNoDisponible` (
  `idHorarioNoDisponible` int NOT NULL AUTO_INCREMENT,
  `idFechaNoDisponible` int NOT NULL,
  `horaInicio` time NOT NULL,
  `horaFin` time NOT NULL,
  PRIMARY KEY (`idHorarioNoDisponible`),
  KEY `fk_HorarioNoDisponible_FechaNoDisponible_idx` (`idFechaNoDisponible`),
  CONSTRAINT `fk_HorarioNoDisponible_FechaNoDisponible` FOREIGN KEY (`idFechaNoDisponible`) REFERENCES `FechaNoDisponible` (`idFechaNoDisponible`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Modalidad`
--

DROP TABLE IF EXISTS `Modalidad`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Modalidad` (
  `idModalidad` int NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`idModalidad`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Reserva`
--

DROP TABLE IF EXISTS `Reserva`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Reserva` (
  `idReserva` int NOT NULL AUTO_INCREMENT,
  `idTipoReserva` int NOT NULL,
  `idCliente` int NOT NULL,
  `fechaReservaTemporal` datetime DEFAULT NULL,
  `fechaReserva` date DEFAULT NULL,
  `horaInicio` time DEFAULT NULL,
  `horaFin` time DEFAULT NULL,
  `idFormaPago` int DEFAULT NULL,
  `fechaPago` datetime DEFAULT NULL,
  `idDescuento` int DEFAULT NULL,
  `montoPago` decimal(8,2) DEFAULT NULL,
  `idExamen` int DEFAULT NULL,
  `idUsuario` int DEFAULT NULL,
  `idConsultorio` int DEFAULT NULL,
  `cEstado` char(1) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`idReserva`),
  KEY `fk_Reserva_TipoReserva_idx` (`idTipoReserva`),
  KEY `fk_Reserva_Cliente_idx` (`idCliente`),
  KEY `fk_Reserva_FormaPago_idx` (`idFormaPago`),
  KEY `fk_Reserva_Examen_idx` (`idExamen`),
  KEY `fk_Reserva_Descuento_idx` (`idDescuento`),
  KEY `fk_Reserva_Medico_idx` (`idUsuario`),
  KEY `fk_Reserva_Consultorio_idx` (`idConsultorio`),
  CONSTRAINT `fk_Reserva_Cliente` FOREIGN KEY (`idCliente`) REFERENCES `Cliente` (`idCliente`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_Reserva_Consultorio` FOREIGN KEY (`idConsultorio`) REFERENCES `Consultorio` (`idConsultorio`),
  CONSTRAINT `fk_Reserva_Descuento` FOREIGN KEY (`idDescuento`) REFERENCES `Descuento` (`idDescuento`),
  CONSTRAINT `fk_Reserva_Examen` FOREIGN KEY (`idExamen`) REFERENCES `Examen` (`idExamen`),
  CONSTRAINT `fk_Reserva_FormaPago` FOREIGN KEY (`idFormaPago`) REFERENCES `FormaPago` (`idFormaPago`),
  CONSTRAINT `fk_Reserva_Medico` FOREIGN KEY (`idUsuario`) REFERENCES `Usuario` (`idUsuario`),
  CONSTRAINT `fk_Reserva_TipoReserva` FOREIGN KEY (`idTipoReserva`) REFERENCES `TipoReserva` (`idTipoReserva`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Rol`
--

DROP TABLE IF EXISTS `Rol`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Rol` (
  `idRol` int NOT NULL AUTO_INCREMENT,
  `titulo` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descripcion` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`idRol`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `TipoConsultorio`
--

DROP TABLE IF EXISTS `TipoConsultorio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `TipoConsultorio` (
  `idTipoConsultorio` int NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`idTipoConsultorio`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `TipoReserva`
--

DROP TABLE IF EXISTS `TipoReserva`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `TipoReserva` (
  `idTipoReserva` int NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `intervalo` int NOT NULL,
  `cantidad` int NOT NULL,
  `rangoInicio` time NOT NULL,
  `rangoFinal` time NOT NULL,
  PRIMARY KEY (`idTipoReserva`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Usuario`
--

DROP TABLE IF EXISTS `Usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Usuario` (
  `idUsuario` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `apellido` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descripcion` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `usuario` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contrasena` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `idRol` int NOT NULL,
  `idEspecialidad` int DEFAULT NULL,
  `idModalidad` int DEFAULT NULL,
  PRIMARY KEY (`idUsuario`),
  KEY `fk_Usuario_Rol_idx` (`idRol`),
  KEY `fk_Usuario_Especialidad_idx` (`idEspecialidad`),
  KEY `fk_Usuario_Modalidad_idx` (`idModalidad`),
  CONSTRAINT `fk_Usuario_Especialidad` FOREIGN KEY (`idEspecialidad`) REFERENCES `Especialidad` (`idEspecialidad`),
  CONSTRAINT `fk_Usuario_Modalidad` FOREIGN KEY (`idModalidad`) REFERENCES `Modalidad` (`idModalidad`),
  CONSTRAINT `fk_Usuario_Rol` FOREIGN KEY (`idRol`) REFERENCES `Rol` (`idRol`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping routines for database 'ReservaMedica'
--
/*!50003 DROP PROCEDURE IF EXISTS `LISTADO_TIPORESERVA` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `LISTADO_TIPORESERVA`(
 IN _opcion INT
)
BEGIN
   IF _opcion=1 THEN
      select  idExamen AS id , descripcion AS nombre from Examen ;
      
   END IF;
   IF _opcion=2 THEN
      select  idUsuario AS id, concat(nombre,' ', apellido ) AS nombre from Usuario 
      where idEspecialidad is not null and idModalidad is not null;
   END IF;
   IF _opcion=3 THEN
      select  idConsultorio AS id, nombre from Consultorio ;
   END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `MANTENIMIENTO_CALENDARIO` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `MANTENIMIENTO_CALENDARIO`(
 IN _opcion int ,
 IN _id int,
 IN _fecha DATE,
 IN _habilitado char(1)
)
BEGIN
	IF _opcion=1 THEN
        
		SELECT idCalendarioNoLaborable,DATE_FORMAT(fecha, "%Y-%m-%d") as fecha ,habilitado FROM CalendarioNoLaborable
        WHERE fecha>=_fecha;
        
    END IF;
     
	IF _opcion=2 THEN
		INSERT INTO CalendarioNoLaborable(fecha,habilitado)
        VALUES (_fecha,_habilitado);
	END IF;
    
    IF _opcion=3 THEN
		DELETE FROM CalendarioNoLaborable
        WHERE fecha = _fecha;
	END IF;
    
  
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `MANTENIMIENTO_CLIENTE` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `MANTENIMIENTO_CLIENTE`(
 IN _opcion INT,
 IN _idCliente INT,
 IN _nombre VARCHAR(45),
 IN _apellido VARCHAR(45),
 IN _email VARCHAR(45),
 IN _telefono VARCHAR(45),
 IN _dni VARCHAR(8),
 IN _distrito VARCHAR(45),
 IN _edad INT,
 IN _ruc VARCHAR(45),
 IN _razonSocial VARCHAR(45)
)
BEGIN
	IF _opcion=1 THEN
		
        INSERT INTO Cliente (nombre,apellido,email,telefono)
        VALUES (_nombre,_apellido,_email,_telefono);
        
        SELECT max(idCliente) as idCliente FROM Cliente;
        
    END IF;
    
    
    IF _opcion=2 THEN
		UPDATE Cliente
        SET dni = _dni,
        distrito=_distrito,
        edad= _edad,
        ruc = _ruc,
        razonSocial= _razonSocial
        WHERE idCliente = _idCliente;
    END IF ;
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `MANTENIMIENTO_CONSULTAS` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `MANTENIMIENTO_CONSULTAS`(
 IN fechaInicio date,
 IN fechaFin date,
 IN _cEstado char(1)
)
BEGIN
	
    SELECT 
    r.idReserva,
    r.idTipoReserva,
    r.idCliente,
    r.idExamen,
    ifnull(e.descripcion, ifnull(concat(u.nombre,' ', u.apellido), ifnull(con.nombre,''))) AS descripcionServicio,
    DATE_FORMAT(r.fechaReservaTemporal, "%Y-%m-%d %H:%i:%S") as fechaReservaTemporal,
    DATE_FORMAT(r.fechaReserva, "%Y-%m-%d") as fechaReserva,
    r.horaInicio,
    r.horaFin,
    r.montoPago,
    case 
    when r.cEstado='I' THEN 'Incompleto'
    else 'Completo' end AS cEstado,
    tr.descripcion AS descripcionTipoReserva,
    c.nombre as nombreCliente,
    c.apellido as apellidoCliente,
    c.email as emailCliente,
    ifnull(c.dni,'') as dniCliente,
    ifnull(fp.descripcion,'') as descripcionFormaPago
    FROM 
    Reserva r
    INNER JOIN TipoReserva tr on tr.idTipoReserva=r.idTipoReserva
    INNER JOIN Cliente c on c.idCliente = r.idCliente
    LEFT JOIN Examen e on e.idExamen = r.idExamen
    LEFT JOIN Usuario u on u.idUsuario = r.idUsuario
    LEFT JOIN Consultorio con on con.idConsultorio=r.idConsultorio
    LEFT JOIN FormaPago fp on fp.idFormaPago = r.idFormaPago
    WHERE ((r.fechaReserva>=fechaInicio and r.fechaReserva<=fechaFin) OR r.fechaReserva IS NULL) 
    AND (r.cEstado = _cEstado or _cEstado='')
    order by r.fechaReservaTemporal desc;
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `MANTENIMIENTO_CONSULTORIO` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `MANTENIMIENTO_CONSULTORIO`(
 IN _opcion INT,
 IN _idConsultorio INT,
 IN _nombre VARCHAR(255),
 IN _monto DECIMAL(8,2),
 IN _idTipoConsultorio INT
)
BEGIN


	IF _opcion=1 THEN
		select 
        c.idConsultorio,
        c.nombre as nombreConsultorio,
        c.monto,
        c.idTipoConsultorio,
        tc.descripcion as nombreTipoConsultorio
        FROM Consultorio c 
        INNER JOIN TipoConsultorio tc on tc.idTipoConsultorio=c.idTipoConsultorio;
    END IF;
	IF _opcion=2 THEN
		
        INSERT INTO Consultorio(nombre,monto,idTipoConsultorio)
        values (_nombre,_monto,_idTipoConsultorio);
        
    END IF;
    
    IF _opcion=3 THEN
    
		UPDATE Consultorio
        SET nombre=_nombre,
			monto =_monto,
            idTipoConsultorio=_idTipoConsultorio
		where idConsultorio = _idConsultorio;
    END IF;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `MANTENIMIENTO_DESCUENTO` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `MANTENIMIENTO_DESCUENTO`(
 IN _opcion int,
 IN _idDescuento int,
 IN _nombre varchar(45),
 IN _descripcion varchar(255),
 IN _fechaInicio  date,
 IN _fechaFin date,
 IN _valor decimal(5,2),
 IN _habilitado char(1),
 IN _idTipoReserva int,
 IN _tipoDescuento CHAR(1),
 IN _fechaConsulta date
)
BEGIN
	
    IF _opcion=1 THEN
    
		INSERT INTO Descuento (nombre,descripcion,fechaInicio,fechaFin,valor,habilitado,idTipoReserva,tipoDescuento)
        VALUES (_nombre,_descripcion,_fechaInicio,_fechaFin,_valor,_habilitado,_idTipoReserva,_tipoDescuento);
		
    END IF;
    
    IF _opcion=2 THEN
		UPDATE Descuento 
        SET nombre=_nombre,
        descripcion=_descripcion,
        fechaInicio=_fechaInicio,
        fechaFin = _fechaFin,
        valor = _valor,
        habilitado = _habilitado,
        idTipoReserva= _idTipoReserva,
        tipoDescuento = _tipoDescuento
        WHERE idDescuento = _idDescuento;
    END IF;
    
    IF _opcion=3 THEN
		
		SELECT idDescuento,valor FROM Descuento
        WHERE 
        idTipoReserva = _idTipoReserva
        AND nombre = _nombre
        AND habilitado='1'
        AND ((tipoDescuento='I') OR (tipoDescuento='M' AND fechaInicio<=_fechaConsulta AND fechaFin>=_fechaConsulta ) );
        
        
    
    END IF;
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `MANTENIMIENTO_ESPECIALIDAD` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `MANTENIMIENTO_ESPECIALIDAD`(
 IN _opcion INT,
 IN _idEspecialidad INT,
 IN _descripcion VARCHAR(255),
 IN _monto DECIMAL(8,2)
)
BEGIN
	IF _opcion=0 THEN
		select idModalidad,descripcion as descripcionModalidad from Modalidad;
    END IF;
    
	IF _opcion=1 THEN
    
		SELECT idEspecialidad,descripcion as descripcionEspecialidad,monto FROM Especialidad;
    END IF;
	
    IF _opcion = 2 THEN
		insert into Especialidad (descripcion,monto)
        values (_descripcion,_monto);
    END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `MANTENIMIENTO_EXAMEN` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `MANTENIMIENTO_EXAMEN`(
 IN _opcion int ,
 IN _id int,
 IN _descripcion varchar(255),
 IN _monto decimal(8,2)
)
BEGIN
	IF _opcion=1 THEN
        
		select idExamen, descripcion,monto from Examen;
    END IF;
     
	IF _opcion=2 THEN
		select idExamen, descripcion,monto from Examen
        where idExamen=_id;
	END IF;
    IF _opcion=3 THEN
		insert into Examen(descripcion,monto)
        values (_descripcion,_monto);
        select max(idExamen) as idExamen from Examen;
	END IF;
    IF _opcion=4 THEN
		update Examen
        set descripcion= _descripcion,
		monto = _monto
        where idExamen=_id;
	END IF;
    IF _opcion=5 THEN
		delete from Examen where idExamen=_id;
	END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `MANTENIMIENTO_FECHANODISPONIBLE` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `MANTENIMIENTO_FECHANODISPONIBLE`(
IN _opcion int,
IN _idFechaNoDisponible int,
IN _idTipoReserva int,
IN _idEscogido int,
IN _anio int,
IN _mes int,
IN _semana int,
IN _dia int
)
BEGIN
	IF _opcion=0 THEN
		IF _idTipoReserva=1 THEN
			DELETE FROM FechaNoDisponible
            WHERE idTipoReserva=_idTipoReserva and idExamen=_idEscogido AND anio=_anio AND mes=_mes and semana=_semana AND dia=_dia;
        END IF;
        IF _idTipoReserva=2 THEN
			DELETE FROM FechaNoDisponible
            WHERE idTipoReserva=_idTipoReserva and idUsuario=_idEscogido AND anio=_anio AND mes=_mes and semana=_semana AND dia=_dia;
        END IF;
        IF _idTipoReserva=3 THEN
			DELETE FROM FechaNoDisponible
            WHERE idTipoReserva=_idTipoReserva and idConsultorio=_idEscogido AND anio=_anio AND mes=_mes and semana=_semana AND dia=_dia;
        END IF;
    END IF;
	IF _opcion=1 THEN
		IF _idTipoReserva=1 THEN
			INSERT INTO FechaNoDisponible(idTipoReserva,idExamen,anio,mes,semana,dia)
            VALUES (_idTipoReserva,_idEscogido,_anio,_mes,_semana,_dia);
        END IF;
        IF _idTipoReserva=2 THEN
			INSERT INTO FechaNoDisponible(idTipoReserva,idUsuario,anio,mes,semana,dia)
            VALUES (_idTipoReserva,_idEscogido,_anio,_mes,_semana,_dia);
        END IF;
        IF _idTipoReserva=3 THEN
			INSERT INTO FechaNoDisponible(idTipoReserva,idConsultorio,anio,mes,semana,dia)
            VALUES (_idTipoReserva,_idEscogido,_anio,_mes,_semana,_dia);
        END IF;
        
        select max(idFechaNoDisponible) as id from FechaNoDisponible;
    END IF;
    
    IF _opcion=2 THEN
		IF _idTipoReserva = 1 THEN
			SELECT 
			fnd.idFechaNoDisponible,
			fnd.dia,
			hnd.idHorarioNoDisponible,
			hnd.horaInicio,
			hnd.horaFin
			FROM
			FechaNoDisponible fnd
			INNER JOIN HorarioNoDisponible hnd on hnd.idFechaNoDisponible=fnd.idFechaNoDisponible
			WHERE fnd.idTipoReserva=_idTipoReserva and fnd.idExamen=_idEscogido and fnd.anio=_anio and fnd.mes=_mes and fnd.semana=_semana;
        END IF;
        IF _idTipoReserva = 2 THEN
			SELECT 
			fnd.idFechaNoDisponible,
			fnd.dia,
			hnd.idHorarioNoDisponible,
			hnd.horaInicio,
			hnd.horaFin
			FROM
			FechaNoDisponible fnd
			INNER JOIN HorarioNoDisponible hnd on hnd.idFechaNoDisponible=fnd.idFechaNoDisponible
			WHERE fnd.idTipoReserva=_idTipoReserva and fnd.idUsuario=_idEscogido and fnd.anio=_anio and fnd.mes=_mes and fnd.semana=_semana;
        END IF;
        IF _idTipoReserva = 3 THEN
			SELECT 
			fnd.idFechaNoDisponible,
			fnd.dia,
			hnd.idHorarioNoDisponible,
			hnd.horaInicio,
			hnd.horaFin
			FROM
			FechaNoDisponible fnd
			INNER JOIN HorarioNoDisponible hnd on hnd.idFechaNoDisponible=fnd.idFechaNoDisponible
			WHERE fnd.idTipoReserva=_idTipoReserva and fnd.idConsultorio=_idEscogido and fnd.anio=_anio and fnd.mes=_mes and fnd.semana=_semana;
        END IF;
    END IF;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `MANTENIMIENTO_FORMAPAGO` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `MANTENIMIENTO_FORMAPAGO`(
 IN _opcion INT,
 IN _idFormaPago INT,
 IN _descripcion VARCHAR(45),
 IN _habilitado CHAR(1)
)
BEGIN

	IF _opcion=1 THEN
		
        INSERT INTO FormaPago (descripcion,habilitado)
        VALUES (_descripcion,_habilitado);
        
    END IF;
    
    IF _opcion=2 THEN
		UPDATE FormaPago
        SET descripcion = _descripcion,
        habilitado = _habilitado
        WHERE idFormaPago = _idFormaPago;
    END IF;
    
    IF _opcion=3 THEN
		
        SELECT idFormaPago,descripcion,habilitado 
        FROM FormaPago;
        
    END IF;
    
    

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `MANTENIMIENTO_HORARIO` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `MANTENIMIENTO_HORARIO`(
 IN _opcion INT,
 IN _idHorarioNoDisponible INT,
 IN _idFechaNoDisponible INT,
 IN _horaInicio TIME,
 IN _horaFin TIME
)
BEGIN
  IF _opcion=0 THEN
	 DELETE FROM HorarioNoDisponible 
	 WHERE idFechaNoDisponible= _idFechaNoDisponible;
  END IF;
  IF _opcion=1 THEN
    
    
	INSERT INTO HorarioNoDisponible (idFechaNoDisponible,horaInicio,horaFin)
    VALUES (_idFechaNoDisponible,_horaInicio,_horaFin);
  END IF;
  
	
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `MANTENIMIENTO_OBTENERFECHASDISPONIBLES` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `MANTENIMIENTO_OBTENERFECHASDISPONIBLES`(
 IN _idTipoReserva INT,
 IN _idEscogido INT,
 IN _fecha DATE
)
BEGIN

	declare _cantidad int;
    declare _intervalo int;
	declare _rangoInicio time;
    declare _rangoFinalSabado time;
    declare _rangoFinal time;
    declare _totalRango int;
    declare _totalRangoSabado int;
    declare _cantidadIntervalos int default 0;
    declare _cantidadIntervalosSabado int default 0;
    declare _cantidadIntervaloNoDisponibles int default 0;
    
    set _rangoInicio = (select rangoInicio from TipoReserva where idTipoReserva=_idTipoReserva);
    set _rangoFinalSabado= '14:00:00';
    set _rangoFinal = (select rangoFinal from TipoReserva where idTipoReserva=_idTipoReserva);
    set _totalRango = floor(time_to_sec(timediff(_rangoFinal,_rangoInicio))/60);
	set _totalRangoSabado = floor(time_to_sec(timediff(_rangoFinalSabado,_rangoInicio))/60);
    set _cantidad = (select cantidad from TipoReserva where idTipoReserva=_idTipoReserva);
    set _intervalo = (select intervalo from TipoReserva where idTipoReserva=_idTipoReserva);
    set _cantidadIntervalos =floor(_totalRango/_intervalo);
    set _cantidadIntervalosSabado =floor(_totalRangoSabado/_intervalo);

   
    select 
    r.idTipoReserva,
    r.idExamen,
    DATE_FORMAT(r.fechaReserva, "%Y-%m-%d") as fechaReserva, 
    count(1) as cantidadReservasSegunFecha,
    case (SELECT COUNT(1) FROM FechaNoDisponible fnd
     WHERE 
     fnd.idTipoReserva = r.idTipoReserva AND
     fnd.idExamen = r.idExamen AND
     fnd.anio = extract(YEAR FROM fechaReserva) AND
     fnd.mes = extract(MONTH FROM fechaReserva) AND
     fnd.semana=  WEEK(fechaReserva, 0) - WEEK(DATE_SUB(fechaReserva, INTERVAL DAYOFMONTH(fechaReserva) - 1 DAY), 0) + 1 AND
	 fnd.dia = dayofweek(fechaReserva) - 1 
     ) 
     when 0 THEN 
		case dayofweek(fechaReserva) - 1 
         when 0 then 
         0
         when 6 then
         _cantidadIntervalosSabado*_cantidad
         else
         _cantidadIntervalos*_cantidad
		end
     ELSE 
	( _cantidadIntervalos
        - 
		(SELECT COUNT(1) FROM FechaNoDisponible fnd
		 INNER JOIN HorarioNoDisponible hnd ON fnd.idFechaNoDisponible=hnd.idFechaNoDisponible
		 WHERE 
		 fnd.idTipoReserva = r.idTipoReserva AND
		 fnd.idExamen = r.idExamen AND
		 fnd.anio = extract(YEAR FROM fechaReserva) AND
		 fnd.mes = extract(MONTH FROM fechaReserva) AND
		 fnd.semana=  WEEK(fechaReserva, 0) - WEEK(DATE_SUB(fechaReserva, INTERVAL DAYOFMONTH(fechaReserva) - 1 DAY), 0) + 1 AND
		 fnd.dia = dayofweek(fechaReserva) - 1 
		 ) 
	)*_cantidad
     
	end as  cantidadTopeSegunFecha
    from Reserva r
   
    where r.fechaReserva>=_fecha
    AND r.idTipoReserva=_idTipoReserva
	AND ((r.idExamen = _idEscogido AND _idTipoReserva=1) or (r.idUsuario = _idEscogido AND _idTipoReserva=2) or (r.idConsultorio = _idEscogido AND _idTipoReserva=3) ) 
    AND r.cEstado = 'C'
    group by r.idTipoReserva,r.idExamen,r.fechaReserva
	having cantidadReservasSegunFecha >= cantidadTopeSegunFecha;
    
    
	SELECT fnd.anio,fnd.mes,fnd.semana,fnd.dia,_cantidadIntervalos as cantidadIntervalosTotal, count(1) as cantidadIntervalosNoDisponibles
    FROM FechaNoDisponible fnd
    INNER JOIN HorarioNoDisponible hnd ON fnd.idFechaNoDisponible=hnd.idFechaNoDisponible
    WHERE 
    fnd.idTipoReserva = _idTipoReserva AND
	((fnd.idExamen = _idEscogido AND _idTipoReserva=1) or (fnd.idUsuario = _idEscogido AND _idTipoReserva=2) or (fnd.idConsultorio = _idEscogido AND _idTipoReserva=3) ) AND
	fnd.anio >= extract(YEAR FROM _fecha) AND
	fnd.mes >= extract(MONTH FROM _fecha) AND
	fnd.semana>=  WEEK(_fecha, 0) - WEEK(DATE_SUB(_fecha, INTERVAL DAYOFMONTH(_fecha) - 1 DAY), 0)
    group by fnd.anio,fnd.mes,fnd.semana,fnd.dia;
   -- having cantidadIntervalosNoDisponibles>=_cantidadIntervalos;
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `MANTENIMIENTO_OBTENERHORARIONODISPONIBLEPORFECHAELEGIDA` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `MANTENIMIENTO_OBTENERHORARIONODISPONIBLEPORFECHAELEGIDA`(
 IN _idTipoReserva INT,
 IN _idEscogido INT,
 IN _fechaElegida DATE
)
BEGIN

   DECLARE _dia VARCHAR(45);
   declare _cantidad int;
   
   set _dia = CONCAT(ELT(WEEKDAY(_fechaElegida) + 1,'lunes', 'martes', 'miercoles', 'jueves', 'viernes', 'sabado','domingo'));
   set _cantidad = (select cantidad from TipoReserva where idTipoReserva=_idTipoReserva);
   
   select 
    r.horaInicio,
    r.horaFin,
    count(1) as cantidadReservasSegunHorario
    from Reserva r
   
    where r.fechaReserva=_fechaElegida
    AND r.idTipoReserva=_idTipoReserva
	AND ((r.idExamen = _idEscogido AND _idTipoReserva=1) or (r.idUsuario = _idEscogido AND _idTipoReserva=2) or (r.idConsultorio = _idEscogido AND _idTipoReserva=3) ) 
    AND r.cEstado = 'C'
    group by r.horaInicio,r.horaFin
    having cantidadReservasSegunHorario>= _cantidad;
    
    SELECT hnd.horaInicio,hnd.horaFin FROM FechaNoDisponible fnd
    INNER JOIN HorarioNoDisponible hnd on hnd.idFechaNoDisponible=fnd.idFechaNoDisponible
    WHERE 
    fnd.idTipoReserva = _idTipoReserva AND
	((fnd.idExamen = _idEscogido AND _idTipoReserva=1) or (fnd.idUsuario = _idEscogido AND _idTipoReserva=2) or (fnd.idConsultorio = _idEscogido AND _idTipoReserva=3) )  AND
	fnd.anio = extract(YEAR FROM _fechaElegida) AND
	fnd.mes = extract(MONTH FROM _fechaElegida) AND
	fnd.semana=  WEEK(_fechaElegida, 0) - WEEK(DATE_SUB(_fechaElegida, INTERVAL DAYOFMONTH(_fechaElegida) - 1 DAY), 0) + 1 AND
    fnd.dia = dayofweek(_fechaElegida) - 1 ;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `MANTENIMIENTO_RESERVA` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `MANTENIMIENTO_RESERVA`(
 IN _opcion INT,
 IN _idReserva INT,
 IN _idTipoReserva INT,
 IN _idCliente INT,
 IN _fechaTemporal DATETIME,
 IN _fechaReserva DATE,
 IN _horaInicio TIME,
 IN _horaFin TIME,
 IN _idFormaPago INT,
 IN _fechaPago DATETIME,
 IN _idDescuento INT,
 IN _montoPago  DECIMAL(8,2),
 IN _idExamen INT,
 IN _idUsuario INT,
 IN _idConsultorio INT,
 IN _cEstado CHAR(1)
)
BEGIN
  declare _idParaCliente int;
  IF _opcion = 1 THEN
		IF _idTipoReserva=1 THEN
            if _idReserva= 0 then
				INSERT INTO Reserva(idTipoReserva,idCliente,fechaReservaTemporal,idExamen,cEstado)
				VALUES (_idTipoReserva,_idCliente,_fechaTemporal,_idExamen,_cEstado);
            end if;
			if _idReserva<>0 then
				set _idParaCliente = (SELECT idCliente from Reserva where idReserva=_idReserva);
                INSERT INTO Reserva(idTipoReserva,idCliente,fechaReservaTemporal,idExamen,cEstado)
				VALUES (_idTipoReserva,_idParaCliente,_fechaTemporal,_idExamen,_cEstado);
            end if;
        END IF;
		IF _idTipoReserva=2 THEN
			if _idReserva= 0 then
				INSERT INTO Reserva(idTipoReserva,idCliente,fechaReservaTemporal,idUsuario,cEstado)
				VALUES (_idTipoReserva,_idCliente,_fechaTemporal,_idUsuario,_cEstado);
            end if;
			if _idReserva<>0 then
				set _idParaCliente = (SELECT idCliente from Reserva where idReserva=_idReserva);
                INSERT INTO Reserva(idTipoReserva,idCliente,fechaReservaTemporal,idUsuario,cEstado)
				VALUES (_idTipoReserva,_idParaCliente,_fechaTemporal,_idUsuario,_cEstado);
            end if;
			
        END IF;
        IF _idTipoReserva=3 THEN
			if _idReserva= 0 then
				INSERT INTO Reserva(idTipoReserva,idCliente,fechaReservaTemporal,idConsultorio,cEstado)
				VALUES (_idTipoReserva,_idCliente,_fechaTemporal,_idConsultorio,_cEstado);
            end if;
			if _idReserva<>0 then
				set _idParaCliente = (SELECT idCliente from Reserva where idReserva=_idReserva);
                INSERT INTO Reserva(idTipoReserva,idCliente,fechaReservaTemporal,idConsultorio,cEstado)
				VALUES (_idTipoReserva,_idParaCliente,_fechaTemporal,_idConsultorio,_cEstado);
            end if;
			
        END IF;
        
        SELECT max(idReserva) AS idReserva FROM Reserva;
  END IF;
  
   IF _opcion = 2 THEN

	begin
		declare _montoAPagar decimal(8,2) default 0.0;
        declare _valorDescuento int default 0;
        
		IF _idTipoReserva=1 THEN
        
			IF _idDescuento = 0 THEN
				set _montoAPagar = (SELECT monto FROM Examen WHERE idExamen = _idExamen);
				
				UPDATE Reserva
				SET fechaReserva=_fechaReserva,
				horaInicio=_horaInicio,
				horaFin=_horaFin,
				montoPago = _montoAPagar,
				idExamen = _idExamen,
				cEstado=_cEstado,
                fechaReservaTemporal= _fechaTemporal
				WHERE idReserva= _idReserva;
            END IF;
            
            IF _idDescuento <> 0 THEN
				set _montoAPagar = (SELECT monto FROM Examen WHERE idExamen = _idExamen);
				set _valorDescuento = (SELECT valor FROM Descuento WHERE idDescuento = _idDescuento);
                set _montoAPagar = (1 - _valorDescuento)*100*_montoAPagar; 
				UPDATE Reserva
				SET fechaReserva=_fechaReserva,
				horaInicio=_horaInicio,
				horaFin=_horaFin,
				montoPago = _montoAPagar,
				idExamen = _idExamen,
                idDescuento = _idDescuento,
				cEstado=_cEstado,
                fechaReservaTemporal= _fechaTemporal
				WHERE idReserva= _idReserva;
            END IF;
			
            SELECT idCliente FROM Reserva where idReserva= _idReserva;
        END IF;
		IF _idTipoReserva=2 THEN
        
           
			IF _idDescuento = 0 THEN
				set _montoAPagar = (SELECT e.monto FROM Usuario u LEFT JOIN Especialidad e on e.idEspecialidad=u.idEspecialidad WHERE u.idUsuario = _idUsuario);
				
				UPDATE Reserva
				SET fechaReserva=_fechaReserva,
				horaInicio=_horaInicio,
				horaFin=_horaFin,
				montoPago = _montoAPagar,
				idUsuario = _idUsuario,
				cEstado=_cEstado,
                fechaReservaTemporal= _fechaTemporal
				WHERE idReserva= _idReserva;
            END IF;
            
            IF _idDescuento <> 0 THEN
				set _montoAPagar = (SELECT e.monto FROM Usuario u LEFT JOIN Especialidad e on e.idEspecialidad=u.idEspecialidad WHERE u.idUsuario = _idUsuario);
				set _valorDescuento = (SELECT valor FROM Descuento WHERE idDescuento = _idDescuento);
                set _montoAPagar = (1 - _valorDescuento)*100*_montoAPagar; 
				UPDATE Reserva
				SET fechaReserva=_fechaReserva,
				horaInicio=_horaInicio,
				horaFin=_horaFin,
				montoPago = _montoAPagar,
				idUsuario = _idUsuario,
                idDescuento = _idDescuento,
				cEstado=_cEstado,
                fechaReservaTemporal= _fechaTemporal
				WHERE idReserva= _idReserva;
            END IF;
			
            SELECT idCliente FROM Reserva where idReserva= _idReserva;
		
        END IF;
        IF _idTipoReserva=3 THEN
			IF _idDescuento = 0 THEN
				set _montoAPagar = (SELECT monto FROM Consultorio WHERE idConsultorio = _idConsultorio);
				
				UPDATE Reserva
				SET fechaReserva=_fechaReserva,
				horaInicio=_horaInicio,
				horaFin=_horaFin,
				montoPago = _montoAPagar,
				idConsultorio = _idConsultorio,
				cEstado=_cEstado,
                fechaReservaTemporal= _fechaTemporal
				WHERE idReserva= _idReserva;
            END IF;
            
            IF _idDescuento <> 0 THEN
				set _montoAPagar = (SELECT monto FROM Consultorio WHERE idConsultorio = _idConsultorio);
				set _valorDescuento = (SELECT valor FROM Descuento WHERE idDescuento = _idDescuento);
                set _montoAPagar = (1 - _valorDescuento)*100*_montoAPagar; 
				UPDATE Reserva
				SET fechaReserva=_fechaReserva,
				horaInicio=_horaInicio,
				horaFin=_horaFin,
				montoPago = _montoAPagar,
				idConsultorio = _idConsultorio,
                idDescuento = _idDescuento,
				cEstado=_cEstado,
                fechaReservaTemporal= _fechaTemporal
				WHERE idReserva= _idReserva;
            END IF;
			
            SELECT idCliente FROM Reserva where idReserva= _idReserva;
        END IF;
        
     end;   
  END IF;
  
	IF _opcion = 3 THEN
    
		Select 
       ifnull( e.descripcion,'') as descripcionExamen,
       ifnull( concat(u.nombre,' ', u.apellido ),'') as descripcionUsuario,
       ifnull( c.nombre,'') as descripcionConsultorio,
        tr.descripcion as descripcionTipoReserva,
        r.montoPago,
        DATE_FORMAT(r.fechaReserva, "%Y-%m-%d") as fechaReserva,
        r.horaInicio,
        r.horaFin
        FROM
        Reserva r 
        INNER JOIN TipoReserva tr on tr.idTipoReserva = r.idTipoReserva
        LEFT JOIN Examen e on e.idExamen= r.idExamen
        LEFT JOIN Usuario u on u.idUsuario=r.idUsuario
        LEFT JOIN Consultorio c on c.idConsultorio=r.idConsultorio
        WHERE r.idReserva = _idReserva;
        
        
    END IF;
    
    IF _opcion=4 THEN
		UPDATE Reserva
        set idFormaPago = _idFormaPago,
        fechaPago= _fechaPago,
        fechaReservaTemporal=_fechaTemporal,
        cEstado=_cEstado
        where idReserva=_idReserva;
    
    END IF; 
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `MANTENIMIENTO_ROLES` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `MANTENIMIENTO_ROLES`(
	IN _opcion int,
    IN _idRol int,
    IN _titulo VARCHAR(45),
    IN _descripcion VARCHAR(45)
)
BEGIN
	
    IF _opcion = 1 THEN
		SELECT idRol,titulo,descripcion FROM Rol;
    END IF;
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `MANTENIMIENTO_TIPOCONSULTORIO` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `MANTENIMIENTO_TIPOCONSULTORIO`(
 IN _opcion int,
 IN _idTipoConsultorio int,
 IN _descripcion varchar(255)
)
BEGIN

	IF _opcion=1 THEN
		select idTipoConsultorio,descripcion From TipoConsultorio;
    END IF;
    
	IF _opcion=2 THEN
		insert into TipoConsultorio (descripcion) values (_descripcion);
    END IF;
    
    IF _opcion=3 THEN
		update TipoConsultorio
        set descripcion = _descripcion
        WHERE idTipoConsultorio=_idTipoConsultorio;
    END IF;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `MANTENIMIENTO_TIPORESERVA` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `MANTENIMIENTO_TIPORESERVA`(
 IN _opcion int,
 IN _id int,
 IN _descripcion varchar(45),
 IN _intervalo int,
 IN _cantidad int ,
 IN _rangoInicio time,
 IN _rangoFinal time
)
BEGIN
	IF _opcion=1 THEN
		select idTipoReserva,descripcion,intervalo,cantidad,rangoInicio,rangoFinal from TipoReserva
        WHERE idTipoReserva=_id or _id=0;
    END IF;
    
    IF _opcion=2 THEN
		INSERT INTO TipoReserva(descripcion,intervalo,cantidad,rangoInicio,rangoFinal)
        VALUE (_descripcion,_intervalo,_cantidad,_rangoInicio,_rangoFinal);
    END IF;
    
    IF _opcion=3 THEN
		UPDATE TipoReserva
        set descripcion=_descripcion,
        intervalo=_intervalo,
        cantidad=_cantidad
        WHERE idTipoReserva=_id;
    END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `MANTENIMIENTO_USUARIO` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `MANTENIMIENTO_USUARIO`(
IN _opcion INT,
IN _idUsuario INT,
IN _nombre VARCHAR(45),
IN _apellido VARCHAR(45),
IN _descripcion VARCHAR(255),
IN _usuario VARCHAR(45),
IN _contrasena VARCHAR(45),
IN _email VARCHAR(45),
IN _idRol INT,
IN _idEspecialidad INT,
IN _idModalidad INT
)
BEGIN

	IF _opcion=1 THEN
		
        SELECT 
        u.idUsuario,
        u.nombre as nombreUsuario,
        u.apellido as apellidoUsuario,
        u.descripcion as descripcionUsuario,
        u.usuario,
        u.contrasena,
        u.email
        FROM Usuario u
        INNER JOIN Rol r on r.idRol=u.idRol
        LEFT JOIN Especialidad e on e.idEspecialidad=u.idEspecialidad
        LEFT JOIN Modalidad m on m.idModalidad=u.idModalidad;
    
    END IF;

	IF _opcion=2 THEN
		
        IF _idRol<>3 THEN
        
			INSERT INTO Usuario (nombre,apellido,descripcion,usuario,contrasena,email,idRol)
            VALUES (_nombre,_apellido,_descripcion,_usuario,_contrasena,_email,_idRol);
        
        END IF;
		
        IF _idRol= 3 THEN
			
            INSERT INTO Usuario (nombre,apellido,descripcion,usuario,contrasena,email,idRol,idEspecialidad,idModalidad)
            VALUES (_nombre,_apellido,_descripcion,_usuario,_contrasena,_email,_idRol,_idEspecialidad,_idModalidad);
			
        END IF;
        
    END IF;


END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-08-16 22:34:19
