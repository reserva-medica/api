import '@babel/polyfill'

import app from './server/server'
import {db} from './server/database'

const main = async ()=>{
    try{
        await app.listen(app.get("port"))
        console.log("server corriendo en el puerto",app.get("port"))
        await db.connect()
        console.log("base de datos conectada")
    }catch(e){
        console.log(e)
    }
    
}

main()