
import { Router } from "express";
import { calendarioController } from "../controllers/calendario.controller";

const router = Router()

router.post('/calendario',calendarioController.registrarDiaNoLaborable)
router.get('/calendario',calendarioController.obtenerDiasNoLaborables)

export default router