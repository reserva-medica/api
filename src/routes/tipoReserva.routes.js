
import { Router } from "express";
import { tipoReservaController } from "../controllers/tipoReserva.controller";

const router = Router();

router.get('/tipoReserva',tipoReservaController.listarTipoReserva)
router.get('/tipoReserva/escogido/:id',tipoReservaController.listarTipoReservaSegunEscogido)
router.get('/tipoReserva/:opcion',tipoReservaController.listarRegistrosPorTipoReserva)
router.post('/tipoReserva',tipoReservaController.registrarTipoReserva)
router.put('/tipoReserva/:id',tipoReservaController.actualizarTipoReserva)

export default router