import { Router } from "express";
import { formaPagoController } from "../controllers/formaPago.controller";

const router = Router()


router.get('/formaPago',formaPagoController.obtenerFormaPagos)

export default router