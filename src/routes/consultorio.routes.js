

import { Router } from 'express'
import { consultorioController } from '../controllers/consultorio.controller'

const router = Router()

router.get('/consultorio',consultorioController.obtenerConsultorios)
router.get('/consultorio/tipo',consultorioController.obtenerTipoConsultorios)
router.post('/consultorio',consultorioController.registrarConsultorio)
router.post('/consultorio/tipo',consultorioController.registrarTipoConsultorio)

export default router