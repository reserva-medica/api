

import { Router } from 'express'
import { usuarioController } from '../controllers/usuario.controller'

const router = Router()

router.get('/usuario/roles',usuarioController.obtenerRoles)
router.get('/usuario/modalidades',usuarioController.obtenerModalidades)
router.get('/usuario/especialidades',usuarioController.obtenerEspecialidades)
router.post('/usuario',usuarioController.registrarUsuario)
router.post('/usuario/especialidad',usuarioController.registrarEspecialidad)

export default router