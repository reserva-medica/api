
import {Router} from 'express'

import { reservaController } from '../controllers/reserva.controller' 

const router = Router()

router.post('/reserva',reservaController.registrarReserva)
router.post('/reserva/clienteValidado',reservaController.registrarNuevaReserva)
router.post('/reserva/cliente',reservaController.registrarClienteParcial)
router.post('/reserva/fechasNoDisponiblesExamen',reservaController.obtenerFechasNoDisponiblesParaExamenes)
router.post('/reserva/HorarioNoDisponiblesExamenSegunFecha',reservaController.obtenerHorariosNoDisponiblesSegunFechaElegida)
router.post('/reserva/pagos',reservaController.obtenerPagos)
router.post('/reserva/realizarPago',reservaController.realizarPago)
router.post('/reserva/consultaReserva',reservaController.obtenerReservas)

export default router

