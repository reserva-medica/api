import { Router } from "express";
import { horarioController } from "../controllers/horario.controller";

const router = Router()

// router.get('/horario/:idTipoReserva/:idEscogido', horarioController.listarHorariosNoDisponiblePorTipoDeReserva)
router.post('/horario/listarHorarioNoDisponible', horarioController.listarHorariosNoDisponiblePorTipoDeReserva)
router.post('/horario', horarioController.registrarHorarioNoDisponiblePorTipoDeReserva)
// router.delete('/horario/:idTipoReserva/:idEscogido',horarioController.eliminarTodosLosHorariosporTipoReserva)
router.post('/horario/eliminarHorarioNoDisponible',horarioController.eliminarTodosLosHorariosporTipoReserva)
export default router