import {Router} from 'express'
import { examenMedicoController } from '../controllers/examenMedico.controller'
const router = Router()

router.get('/examen',examenMedicoController.listarExamenes)

router.post('/examen',examenMedicoController.registrarExamen)

router.put('/examen/:id',examenMedicoController.actualizarExamen)

router.delete('/examen/:id',examenMedicoController.eliminarExamen)

export default router